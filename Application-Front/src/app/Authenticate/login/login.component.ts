import { Component, OnInit } from '@angular/core';
import { User } from '../models/user.model';
import { NgForm } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  UserForm : User = {
      username : '',
      fullname : '',
      email : '',
      password : '',
      role : ''
  }

  constructor(private userService : UserService , private router : Router) { }

  ngOnInit(): void {
    if(sessionStorage.getItem('token') != null )
         this.router.navigateByUrl('/home');
  }

  login(form : NgForm) {
    this.userService.login(form.value).subscribe(
      (succ:any) => {
        sessionStorage.setItem('token',succ.token);
        this.router.navigateByUrl('/home');
    },
    err  => {
      if(err.status == 401)
      console.log("hh")
        // this.toastr.error("Username Or Password Not Correct !!" , "Authenticate");
    }

    )
  }

}
