import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  readonly baseURL = 'http://localhost:61221/api/Authenticate';

  constructor(private http : HttpClient) { }

  //======Login =====//

  login(form) {
   return this.http.post(this.baseURL + '/Login' , form )
  }

  
  roleIsCorrect(allowedRole){
    let isCorrect = false;
    const token = sessionStorage.getItem('token');
    let payLoad = JSON.parse(window.atob(token.split('.')[1]));
    let userRole = payLoad.role;
    allowedRole.forEach(element => {
          if(userRole == element) {
            isCorrect = true;
            return false;
          }
    });
    return isCorrect;
    
}


}
