import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class UserInterceptor implements HttpInterceptor {

  constructor(private router : Router) {}


  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {

    const token = sessionStorage.getItem('token');

    if(token != null)
    {
      const cloneReq = request.clone({

        headers : request.headers.set('Authorization','Bearer '+token)

      });
      return next.handle(cloneReq).pipe(
        tap(
          succ => {},
          err => {
            if(err.status == 401)
                this.router.navigateByUrl('/');
            else if(err.status == 403)
              //  this.router.navigateByUrl('../forbidden');
              console.log('Error from Interceprot')
          }
        )
      )
    }
    else
        return next.handle(request.clone());
  }
}
