import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';



import { AppRoutingModule, routingComponent } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { UserService } from './Authenticate/services/user.service';
import { UserInterceptor } from './Authenticate/interceptor/user.interceptor';
import { ProfileService } from './Dashboard/profile/services/profile.service';





@NgModule({
  declarations: [
    AppComponent,
    routingComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
    
    
  ],
  providers: [UserService , ProfileService, {
    provide: HTTP_INTERCEPTORS,
    useClass : UserInterceptor ,
    multi : true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
