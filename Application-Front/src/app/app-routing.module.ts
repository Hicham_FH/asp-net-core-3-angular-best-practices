import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './Authenticate/login/login.component';


import { ProfileComponent } from './Dashboard/profile/profile.component';
import { HomeComponent } from './dashboard/home/home.component';
import { UserGuard } from './Authenticate/guards/user.guard';
import { AddUserComponent } from './dashboard/add-user/add-user.component';
import { ForbiddenComponent } from './dashboard/forbidden/forbidden.component';


const routes: Routes = [
  { path : '' , redirectTo :'/login' , pathMatch : 'full'},
  { path : 'login' , component : LoginComponent},
  {path : 'home' , component : HomeComponent , canActivate :[UserGuard] , children : [
    {path : 'profile' , component : ProfileComponent},
    {path : 'addCommercial' , component : AddUserComponent , canActivate : [UserGuard] , data : {AccessRole : ['Admin']}},
    {path : 'forbidden' , component : ForbiddenComponent},
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponent = [LoginComponent , HomeComponent , ProfileComponent , ForbiddenComponent , AddUserComponent]
