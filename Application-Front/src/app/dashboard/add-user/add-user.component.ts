import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile/services/profile.service';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private profileService : ProfileService) { }

  commercialData;

  ngOnInit(): void {

    this.profileService.getAllUsers().subscribe(
      res =>{
        this.commercialData = res;
        console.log(this.commercialData)
      },
      err => {
        console.log(err);
      }
    )

  }

}
