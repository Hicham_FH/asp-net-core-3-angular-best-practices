import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProfileService } from '../profile/services/profile.service';
import { UserService } from 'src/app/Authenticate/services/user.service';
import { User } from 'src/app/Authenticate/models/user.model';
import { UserRoles } from '../models/user-roles.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router : Router , private profileService : ProfileService) { }
  
  profileData;
  
  ngOnInit(): void {
    this.profileService.getProfileInfo().subscribe(
      res => {
        this.profileData = res; 
        console.log(this.profileData)
    },
    err => {}
    ) 
  }
  

  logOut()
  {
    sessionStorage.removeItem('token');
    this.router.navigateByUrl('');
  }

}
