export class UserRoles {
    userID : string;
    username : string;
    fullname : string;
    email : string;
    role : string;
    tel : string;
}
