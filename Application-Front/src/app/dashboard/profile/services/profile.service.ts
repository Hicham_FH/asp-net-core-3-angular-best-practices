import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from 'src/app/Authenticate/models/user.model';
import { UserRoles } from '../../models/user-roles.model';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  readonly baseURL = 'http://localhost:61221/api';

  constructor(private http : HttpClient) { }

  //========Get User Info ====//
    getProfileInfo() {
      return this.http.get<UserRoles>(this.baseURL +'/Dashboard/GetUserByID' );
    }

  //=======Get All User =========//

  getAllUsers() {
    return this.http.get<UserRoles>(this.baseURL + '/Dashboard/GetAllUsers');
  }
 

}
