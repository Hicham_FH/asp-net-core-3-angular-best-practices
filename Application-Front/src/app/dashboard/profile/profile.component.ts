import { Component, OnInit } from '@angular/core';
import { ProfileService } from './services/profile.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  profileData;
  constructor(private profileService : ProfileService) { }

  ngOnInit(): void {

    //====File JS===//
  // $.getScript('src/assets_v2/js/main.js');

    this.profileService.getProfileInfo().subscribe(
      res => {
        this.profileData = res; 
    },
    err => {}
    ) 
  }

}
