﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Application_API_V2.Dtos;
using Application_API_V2.Models;
using Application_API_V2.Repositories;
using Application_API_V2.Settings;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Application_API_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticateController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAuthenticateRepository _repository;
        private readonly ApplicationSettings _settings;

        public AuthenticateController(IMapper mapper, IAuthenticateRepository repository, IOptions<ApplicationSettings> settings)
        {
            _mapper = mapper;
            _repository = repository;
            _settings = settings.Value;

        }


        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(RegisterDto model)
        {
            try
            {
                if (model == null) return NotFound();

                    if(ModelState.IsValid)
                    {
                        model.Username = model.Username.ToLower();

                        if (await _repository.UsernameExist(model.Username))
                            return BadRequest("Username Existe Déja");

                        var role = model.Role;

                        if (await _repository.RoleExist(role))
                            return BadRequest("Role Not Found");

                        var user = _mapper.Map<ApplicationUser>(model);

                        var userCreated = await _repository.Register(user, model.Password, role);

                        return StatusCode(201, new
                        {
                            username = userCreated.UserName,
                            email = userCreated.Email,
                            fullname = userCreated.FullName,
                            role = model.Role
                        });
                    }
                return StatusCode(StatusCodes.Status400BadRequest);
                  
            }
            catch(Exception ex)
            {
                throw ex;
            }
            
        }

        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginDto model)
        {
            var userFromRepo = await _repository.Login(model.Username, model.Password);

            if (userFromRepo != null)
            {

                IdentityOptions options = new IdentityOptions();

                var role = await _repository.GetRolesAsync(userFromRepo);

                var tokenHandler = new JwtSecurityTokenHandler();

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                       {
                            new Claim("UserID",userFromRepo.Id.ToString()),
                            new Claim(options.ClaimsIdentity.RoleClaimType , role.FirstOrDefault())
                       }),

                    Issuer = "https://hichamfh",
                    Audience = "https://hichamfh",
                    Expires = DateTime.Now.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.JWT_Key)), SecurityAlgorithms.HmacSha512Signature),
                };

                var tokenSecret = tokenHandler.CreateToken(tokenDescriptor);

                return Ok(new
                {
                    token = tokenHandler.WriteToken(tokenSecret),
                    expiration = tokenSecret.ValidTo
                });
            }
            return Unauthorized();
        }
    }
}