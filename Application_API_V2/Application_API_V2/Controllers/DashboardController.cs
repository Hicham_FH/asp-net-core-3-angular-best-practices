﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Application_API_V2.Models;
using Application_API_V2.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Application_API_V2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DashboardController : ControllerBase
    {
        private IDashboardRepository _repository;

        public DashboardController(IDashboardRepository repository)
        {
            _repository = repository;
        }

       

        [HttpGet]
        [Authorize]
        [Route("GetUserByID")]
        public async Task<Object> GetUserInfo()
        {
            string userID = User.Claims.FirstOrDefault(u => u.Type == "UserID").Value;

            var user = await _repository.GetUserInfoById(userID);

            return user;
        }


        [HttpGet]
        [Authorize(Roles ="Admin")]
        [Route("GetAllUsers")]
        public IEnumerable<Object> getAllUsers()
        {
            return _repository.getAllUsers();
        }

    }
}