﻿using Application_API_V2.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application_API_V2.Repositories
{
    public class DashboardRepository : IDashboardRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _db;

        public DashboardRepository(UserManager<ApplicationUser> userManager , ApplicationDbContext db)
        {
            _userManager = userManager;
            _db = db;
        }

        public async Task<Object> GetUserInfoById(string userID)
        {
            var user = await _userManager.FindByIdAsync(userID);

            var req = (from ur in _db.UserRoles
                       join r in _db.Roles
                       on ur.RoleId equals r.Id
                       join u in _db.Users
                       on ur.UserId equals u.Id
                       select new
                       {
                           UserID = u.Id,
                           Username = u.UserName,
                           Fullname = u.FullName,
                           Email = u.Email,
                           Role = r.Name,
                           Tel = u.PhoneNumber
                       }).Where(x => x.UserID == userID);

            return req;
        }


        //===========Get All Users ===========//

        public  IEnumerable<Object> getAllUsers()
        {
            var req = (from ur in _db.UserRoles
                      join r in _db.Roles
                      on ur.RoleId equals r.Id
                      join u in _db.Users
                      on ur.UserId equals u.Id
                      select new
                      {
                          Username = u.UserName,
                          Fullname = u.FullName,
                          Email = u.Email,
                          Role = r.Name
                      }).Where(x=>x.Role == "Commercial");

            return req.ToList();
        }
    }
}
