﻿using Application_API_V2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application_API_V2.Repositories
{
    public interface IAuthenticateRepository
    {
        //=======Login Method ===============//
        Task<ApplicationUser> Login(String Username, String Password);

        //========Register Method ==========//
        Task<ApplicationUser> Register(ApplicationUser user, String Password, string role);

        //========Username Exist ==========//
        Task<Boolean> UsernameExist(String Username);

        //=======Role Exist ===========//

        Task<Boolean> RoleExist(String role);

        //===========Get Role =========//

        Task<IList<string>> GetRolesAsync(ApplicationUser user);

    }
}
