﻿using Application_API_V2.Models;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application_API_V2.Repositories
{
    public class AuthenticateRepository : IAuthenticateRepository
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ApplicationDbContext _db;
        private readonly RoleManager<ApplicationRole> _roleManager;

        public AuthenticateRepository(UserManager<ApplicationUser> userManager, ApplicationDbContext db , RoleManager<ApplicationRole> roleManager)
        {
            _userManager = userManager;
            _db = db;
            _roleManager = roleManager;
        }


        //===============Login ================//

        public async Task<ApplicationUser> Login(string Username, string Password)
        {
            var user = await _userManager.FindByNameAsync(Username);
            if (user != null && await _userManager.CheckPasswordAsync(user, Password))
            {
                return user;
            }
            return null;
        }

        //=============Register ================//

        public async Task<ApplicationUser> Register(ApplicationUser user, string Password , string role)
        {
            var result = await _userManager.CreateAsync(user, Password);
            await _userManager.AddToRoleAsync(user, role);
            return user;
        }

        //============Username Existe =========//

        public async Task<bool> UsernameExist(string Username)
        {
            if (await _userManager.FindByNameAsync(Username) != null)
                return true;
            return false;

        }

        //==============Role Exist ==============//

        public async Task<bool> RoleExist(String role)
        {
            if ( await _roleManager.FindByNameAsync(role) != null)
                return false;
            return true;
        }

        //============Get Role ==============//

        public async Task<IList<string>> GetRolesAsync(ApplicationUser user)
        {
            return await _userManager.GetRolesAsync(user);
        }
    }
}
