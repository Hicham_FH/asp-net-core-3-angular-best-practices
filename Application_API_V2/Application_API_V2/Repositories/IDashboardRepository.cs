﻿using Application_API_V2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application_API_V2.Repositories
{
    public interface IDashboardRepository
    {
        //==========Get User By ID ============//
        Task<Object> GetUserInfoById(string userID);

        //==========Get All Users ===========//

        IEnumerable<Object> getAllUsers();
    }
}
