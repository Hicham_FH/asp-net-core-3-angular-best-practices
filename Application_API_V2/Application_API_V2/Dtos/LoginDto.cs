﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application_API_V2.Dtos
{
    public class LoginDto
    {
        public String Username { get; set; }
        public String Password { get; set; }
    }
}
