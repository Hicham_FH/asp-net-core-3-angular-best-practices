﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application_API_V2.Dtos
{
    public class RegisterDto
    {
        public String Username { get; set; }
        public String FullName { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }

        public String Role { get; set; }
    }
}
