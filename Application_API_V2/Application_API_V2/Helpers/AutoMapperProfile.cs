﻿using Application_API_V2.Dtos;
using Application_API_V2.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application_API_V2.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {

            CreateMap<LoginDto, ApplicationUser>();
            CreateMap<RegisterDto, ApplicationUser>();
        }
    }
}
